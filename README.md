A simple notes app I created to try out some Angularjs and Clojure.

## Usage
```bash
lein deps
lein run
```

## License

Copyright (C) 2013
