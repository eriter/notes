(function ($, undefined) {
  'use strict';

  var app = angular.module('notes');

  app.factory('noteStorage', function ($rootScope) {
    var Note = window.Model.extend('/notes', ['body']);
    var notes = window.Collection.fromLocalStorage(Note);

    // Sync with server
    // TODO How often does this need to be done?
    // Is once per page load enough?
    // Is some kind of running background sync service required?
    notes.sync().done(function () {
      $rootScope.$apply();
    });

    return {
      items: notes,
      persistRecord: function (record) {
        if (record.isNew) {
          notes.add(record);
        }

        return record.save();
      },
      deleteRecord: function (record) {
        return notes.deleteRecord(record);
      },
      newRecord: function () {
        return new Note();
      },
      get: function (cid) {
        return notes.getByCid(cid);
      }
    };
  });

  app.factory('popup', function ($rootScope) {
    $rootScope.popup = {};

    // Dismiss popup when the location changes
    $rootScope.$on('$locationChangeStart', function () {
      $rootScope.popup.show = false;
    });

    // Enable dismissing the popup with the back button
    window.addEventListener('popstate', function () {
      $rootScope.popup.show = false;
      $rootScope.$apply();
    });

    // Wrapper that dismisses the popup and restores the history stack
    var wrapper = function (callback) {
      callback = callback || function () {};

      return function () {
        window.history.back();
        return callback.apply(callback, arguments);
      };
    };

    return {
      show: function (popup) {
        popup.done = wrapper(popup.done);
        popup.cancel = wrapper(popup.cancel);

        $rootScope.popup = popup;

        // Allow user to dismiss popup and stay at the same location back
        window.history.pushState(null, '', window.location.href);
      }
    };
  });
})(jQuery);
