(function (undefined) {
  'use strict';

  angular.module('notes', ['contenteditable'])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/notes', { templateUrl: 'templates/note-list'
                                    , controller: 'NoteListCtrl' })
                    .when('/notes/new', { templateUrl: 'templates/note'
                                        , controller: 'NoteNewCtrl' })
                    .when('/notes/:id', { templateUrl: 'templates/note'
                                        , controller: 'NoteDetailCtrl' })
                    .otherwise({ redirectTo: '/notes'});
    }]);
})();
