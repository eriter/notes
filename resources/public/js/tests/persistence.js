/* jshint strict: false, unused: false */
/* globals it, describe, expect, beforeEach, spyOn, Model, Collection, moment */
describe('Persistence module', function () {

  var Message = Model.extend('/messages', ['title', 'body']);
  var obj;

  beforeEach(function () {
    obj = new Message({ title: 'Hello', body: 'Who are you?', author: 'admin' });
  });

  describe('Model.syncWith', function () {
    it('a missing updated_at should update all data', function () {
      obj.syncWith({ title: 'Hello there'
                   , body: '!'
                   , author: 'Someone_else' });

      // All data should be updated
      expect(obj.title).toBe('Hello there');
      expect(obj.body).toBe('!');
      expect(obj.author).toBe('Someone_else');
    });

    it('newer data updated_at should update all data', function () {
      var newDate = moment().add('years', 10).toDate();

      expect(obj.updated_at).toBeLessThan(newDate);

      obj.syncWith({ title: 'Hello there'
                   , body: '!'
                   , author: 'Someone_else'
                   , updated_at: newDate });

      // All data should be updated
      expect(obj.title).toBe('Hello there');
      expect(obj.body).toBe('!');
      expect(obj.author).toBe('Someone_else');
    });

    describe('newer local updated_at', function () {
      var oldDate = moment().subtract('years', 10).toDate();

      it('default date should be greater than oldDate', function () {
        expect(obj.updated_at).toBeGreaterThan(oldDate);
      });

      it('should not update mutable data', function () {
        obj.syncWith({ title: 'do not change'
                     , body: 'should not change'
                     , updated_at: oldDate });

        expect(obj.title).toBe('Hello');
        expect(obj.body).toBe('Who are you?');
      });

      it('should update non-mutable data', function () {
        obj.syncWith({ author: 'impostor'
                     , updated_at: oldDate });

        expect(obj.author).toBe('impostor');
      });
    });
  });

  describe('Model constructor', function () {
    it('should have no changes if persisted=true', function () {
      var obj = new Message({ title: 'hello', body: 'nooo' }, true);
      expect(Object.keys(obj.changes()).length).toBe(0);
    });

    it('should have changes if persisted=false', function () {
      var changes = obj.changes();
      expect(changes.title).toBe('Hello');
      expect(changes.body).toBe('Who are you?');
      expect(Object.keys(changes).length).toBe(2);
    });
  });

  describe('Model.isNew', function () {
    it('an unsaved model should be new', function () {
      expect(obj.isNew).toBe(true);
    });

    it('a saved model should not be new', function () {
      obj.save();
      expect(obj.isNew).toBe(false);
    });
  });

  describe('Model.changes', function () {
    it('should only track changes of mutable attributes', function () {
      var obj = new Message({ title: 'hello', body: 'nooo', author: 'John' }, true);
      obj.author = 'Joe';
      expect(Object.keys(obj.changes()).length).toBe(0);
    });
  });

  describe('Collection.add', function () {
    it('collection.add(new Collection.model(obj)) <=> collection.createRecord(obj)', function () {
      var collectionA = Collection.create(Message);
      var collectionB = Collection.create(Message);
      var data = { title: 'Ohh', body: 'Hmm'};

      collectionA.add(new collectionA.model(_.clone(data)));
      collectionB.createRecord(_.clone(data));

      // cids will never be identical
      delete collectionA[0].cid;
      delete collectionB[0].cid;

      expect(JSON.stringify(collectionA)).toBe(JSON.stringify(collectionB));
    });
  });

  describe('Collection.fromLocalStorage', function () {
    it('records loaded from localStorage should not be automatically marked as persisted', function () {
      var collection = Collection.create(Message);
      collection.createRecord({ body: 'nice body', title: 'localStorage' });
      collection._saveToLocalStorage();

      var changes = Collection.fromLocalStorage(Message)[0].changes();

      expect(changes.body).toBe('nice body');
      expect(changes.title).toBe('localStorage');
      expect(Object.keys(changes).length).toBe(2);
    });
  });

  describe('Collection.sync', function () {
    describe('new records should be marked as persisted', function () {
      spyOn(jQuery, 'ajax').andCallFake(function () {
        return jQuery.Deferred().resolve([{title: 'Test', body: 'Test'}], {}, {});
      });

      var collection = Collection.create(Message);
      collection.sync();

      expect(Object.keys(collection[0].changes()).length).toBe(0);
    });
  });
});
