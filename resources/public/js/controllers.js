(function (undefined) {
  'use strict';

  var app = angular.module('notes');

  app.controller('NoteListCtrl', function ($scope, $location, noteStorage) {
    $scope.notes = noteStorage.items;

    $scope.go = function (path) {
      $location.path('/notes/' + path);
    };
  });

  app.controller('NoteDetailCtrl', function ($scope, $routeParams,
                                             $location, noteStorage) {
    $scope.note = noteStorage.get($routeParams.id);

    if ($scope.note === undefined) {
      $location.path('/notes');
      return;
    }

    $scope.$on('$destroy', function () {
      if (!$scope.note.body) {
        // Remove empty notes
        noteStorage.deleteRecord($scope.note);
      }
      else {
        noteStorage.persistRecord($scope.note);
      }
    });
  });

  app.controller('NoteNewCtrl', function ($scope, noteStorage) {
    $scope.note = noteStorage.newRecord();

    $scope.$on('$destroy', function () {
      if ($scope.note.body) {
        // Don't save empty notes
        noteStorage.persistRecord($scope.note);
      }
    });
  });

  app.controller('ToolbarCtrl', function ($scope, $route, $location, noteStorage, popup) {
    var getNote = function () {
      var routeScope = $route.current.scope;
      return routeScope && routeScope.note;
    };

    $scope.trash = function () {
      popup.show({
        title: 'Delete note?',
        body: 'Are you sure you want to delete this note?',
        button1Text: 'Cancel',
        button2Text: 'Delete',
        show: true,
        done: function () {
          noteStorage.deleteRecord($route.current.scope.note);
          $location.path('/notes');
        }
      });
    };

    $scope.save = function () {
      var record = getNote();

      if (record.isNew) {
        $location.path('/notes/' + record.cid);
      }

      noteStorage.persistRecord(record).always(function () {
        $route.current.scope.$apply();
      });
    };

    $scope.canSave = function () {
      var note = getNote();
      return note && note.body && note.isDirty && note.isDirty('body');
    };

    $scope.canDelete = function () {
      var note = getNote();
      return note !== undefined && !note.isNew;
    };

    $scope.showBackButton = function () {
      return $location.path() !== '/notes';
    };
  });
})();
