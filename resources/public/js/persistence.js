(function ($, undefined) {
  'use strict';

  var assert = window.console.assert.bind(window.console);
  var Model = {};

  Model.extend = function (resource, attributes) {
    var jsonAttributes = attributes.concat(['cid', 'persistedAttributes', 'uri',
                                            'created_at', 'updated_at']);

    var constructor = function (data, persisted) {
      this.persistedAttributes = {};
      this.inFlight = false;

      _.extend(this,
        { updated_at: new Date()
        , created_at: new Date()
        , cid: uuid() }, parseData(data || {}));

      if (persisted) {
        _.extend(this.persistedAttributes, _.pick(this, attributes));
      }

      this.isNew = Object.keys(this.persistedAttributes).length === 0;

      assert(this.created_at instanceof Date,
             'created_at must be a date object or valid date string');
      assert(this.updated_at instanceof Date,
             'updated_at must be a date object or valid date string');
    };

    constructor.resource = resource;

    constructor.prototype.save = function () {
      var changes = this.changes();

      if (Object.keys(changes).length > 0) {
        this.isNew = false;
        this.updated_at = window.moment().toDate();

        // Wait until current request is done
        if (this.inFlight) {
          return this.inFlight.then(this.save.bind(this));
        }

        var options = {
          data: changes,
          type: this.uri ? 'PUT' : 'POST',
          url:  this.uri || resource
        };

        this.inFlight = $.ajax(options).done(function (data, textStatus, jqXHR) {
          this.syncWith(data);
          _.extend(this.persistedAttributes, changes);

          // Retrieve uri to created resource
          if (jqXHR.status === 201) {
            this.uri = urlPath(jqXHR.getResponseHeader('location'));
          }
        }.bind(this)).always(function () {
          this.inFlight = false;
          $(this).trigger('saved');
        }.bind(this));

        return this.inFlight.promise();
      }

      return $.Deferred().resolve().promise();
    };

    constructor.prototype.del = function () {
      if (this.uri) {
        // Wait until current request is done
        if (this.inFlight) {
          return this.inFlight.always(this.del.bind(this));
        }

        this.inFlight = $.ajax({
          url: this.uri,
          type: 'DELETE'
        }).done(function () {
          this.id = undefined;
          this.uri = undefined;
          this.persistedAttributes = {};
        }.bind(this)).always(function () {
          this.inFlight = false;
        }.bind(this));

        return this.inFlight.promise();
      }

      return $.Deferred().resolve().promise();
    };

    constructor.prototype.changes = function () {
      return attributes.reduce(function (acc, attr) {
        if (this[attr] !== this.persistedAttributes[attr]) {
          acc[attr] = this[attr];
        }
        return acc;
      }.bind(this), {});
    };

    constructor.prototype.isDirty = function (attr) {
      return this[attr] !== this.persistedAttributes[attr];
    };

    constructor.prototype.toJSON = function () {
      var obj = _.pick(this, jsonAttributes);
      obj.created_at = window.moment(obj.created_at).format();
      obj.updated_at = window.moment(obj.updated_at).format();

      return obj;
    };

    // Syncs the model with the given data object
    constructor.prototype.syncWith = function (data) {
      data = parseData(data);
      var isDataNewer = data.updated_at === undefined ||
                        data.updated_at >= this.updated_at;

      _.each(data, function (value, key) {
        var isMutableAttribute = _.contains(attributes, key);

        if (!isMutableAttribute) {
          // Non-mutable attributes should always be updated
          this[key] = value;
        }
        else if (isDataNewer) {
          // If there's a conflict in one of the mutable attributes, keep the newest.
          this[key] = this.persistedAttributes[key] = value;
        }
      }.bind(this));
    };

    return constructor;
  };

  // Due to various reasons, we can't subclass
  // an array through simply Collection.prototype = new Array;
  // See: http://perfectionkills.com/how-ecmascript-5-still
  //      -does-not-allow-to-subclass-an-array/
  var Collection = {};
  var methods = {};

  Collection.create = function (model, options) {
    var array = [];

    array.model = model;
    array.options = options || {};
    array.lookup = {};

    array._autoSave = function () {
      if (!array._autoSavePaused) {
        array._saveToLocalStorage();
      }
    };

    return _.extend(array, methods);
  };

  Collection.fromLocalStorage = function (model, key) {
    var collection = Collection.create(model);
    key = key || collection.model.resource;

    var records = JSON.parse(window.localStorage[key] || '[]');

    records.forEach(function (record) {
      collection.createRecord(record);
    });

    return collection;
  };

  methods.add = function (record) {
    assert(record instanceof this.model, 'all records must be of the same model');

    this.lookup[record.cid] = record;
    this.push(record);

    $(record).on('saved', this._autoSave);
  };

  methods.remove = function (record) {
    var index = this.indexOf(record);
    assert(index !== -1, 'record must exist in collection.');

    delete this.lookup[record.cid];
    this.splice(index, 1);

    $(record).off('saved', this._autoSave);
    this._autoSave();
  };

  methods.createRecord = function (data, persisted) {
    data = parseData(data);

    var record = new this.model(data || {}, persisted);
    this.add(record);

    return record;
  };

  methods.deleteRecord = function (record) {
    this.remove(record);
    return record.del();
  };

  methods.getByCid = function (cid) {
    return this.lookup[cid];
  };

  methods._saveToLocalStorage = function () {
    var key = this.options.localStorageKey || this.model.resource;
    window.localStorage[key] = JSON.stringify(this);
  };

  methods._suspendAutoSave = function () {
    this._autoSavePaused = true;
  };

  methods._resumeAutoSave = function () {
    this._autoSavePaused = false;
  };

  // Attempts to sync this collection with the server.
  // New records received will be added to the collection.
  // Old records with an uri not appearing in received collection will be deleted.
  // Matching records (identical uri) will be merged, with the updated_at date determining
  // which copy is kept.
  methods.sync = function () {
    var collection = this;
    var d = $.Deferred();

    $.getJSON(collection.model.resource).done(function (data) {
      var localLookup = createLookup('uri', collection);
      var remoteLookup = createLookup('uri', data);

      // Merge/add new notes
      data.forEach(function (record) {
        var existing = localLookup[record.uri];

        if (existing) {
          existing.syncWith(record);
        }
        else {
          collection.createRecord(record, true);
        }
      });

      // Remove deleted notes
      collection.forEach(function (record) {
        if (record.uri && remoteLookup[record.uri] === undefined) {
          collection.remove(record);
        }
      });

      // Save all records to notify server of local changes
      collection._suspendAutoSave();

      $.when.apply($, _.invoke(collection, 'save')).always(function () {
        collection._saveToLocalStorage();
        collection._resumeAutoSave();
        d.resolve();
      });
    }).fail(d.reject);

    return d.promise();
  };

  window.Model = Model;
  window.Collection = Collection;

  function urlPath(url) {
    return url.match(/:\/\/[^/]+(.+)/)[1];
  }

  function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  function parseData(obj) {
    Object.keys(obj).forEach(function (key) {
      if ((key === 'created_at' || key === 'updated_at') && typeof obj[key] === 'string') {
        obj[key] = window.moment.utc(obj[key]).toDate();
      }
    });

    return obj;
  }

  function createLookup(key, array) {
    return array.reduce(function (acc, obj) {
      if (obj[key] !== undefined) {
        acc[obj[key]] = obj;
      }
      return acc;
    }, {});
  }
})(jQuery);
