(ns notes.db.schema
  (:require [clojure.java.jdbc :as jdbc]))

(def url
  (or (System/getenv "DATABASE_URL")
      ; Pass through toURI for Windows compatability
      (let [file-path (java.io.File. (System/getProperty "user.dir") "dev.db")]
           (clojure.string/replace (str (.toURI file-path)) "file:" "sqlite:"))))

(def tables [
  '(:notes
     [:id :integer "PRIMARY KEY AUTOINCREMENT"]
     [:body :varchar "NOT NULL"]
     [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
     [:updated_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"])])

(defn create-tables []
  (jdbc/with-connection url
    (doseq [t tables]
      (apply jdbc/create-table t))))

(defn -main []
  (print "Creating database structure...") (flush)
  (create-tables)
  (println "Done!"))
