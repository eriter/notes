(ns notes.views.common
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css include-js html5]]
        [hiccup.core :only [html]]))

(defpartial layout [& content]
  (html5
    [:head
      [:title "Notes"]
      [:meta {:charset "utf-8"}]
      [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge,chrome=1"}]
      [:meta {:name "viewport"
              :content "width=device-width, initial-scale=1, maximum-scale=1"}]
      (include-css "http://fonts.googleapis.com/css?family=Ubuntu")
      (include-css "/css/font-awesome.css")
      (include-css "/css/reset.css")
      (include-css "/css/style.css")]
    [:body {:ng-app "notes"}
      (html content)
      (include-js "js/libs/jquery-1.10.2.js")
      (include-js "js/libs/moment.min.js")
      (include-js "js/libs/underscore-min.js")
      (include-js "js/libs/angular.js")
      (include-js "js/libs/angular-contenteditable.js")
      (include-js "js/persistence.js")
      (include-js "js/app.js")
      (include-js "js/controllers.js")
      (include-js "js/services.js")]))
