(ns notes.views.main
  (:use [noir.core :only [defpage post-route]])
  (:require [noir.response :as response]
            [noir.request :as request]
            [cheshire.core :as json]
            [notes.views.common :as common]
            [notes.views.templates :as templates]
            [notes.models.note :as note]))

(defn create-template [[name content]]
  [:script {:type "text/ng-template"
            :id   name}
           content])

(defpage "/" []
  (let [notes (map #(assoc (dissoc % :id)
                           :uri
                           (str "/notes/" (:id %))) (note/all))]
    (apply common/layout
      (conj @templates/templates
            [:div.main {:ng-view true}]
            [:ng-include {:src "'templates/popup'"} ""]
            [:ng-include {:src "'templates/nav'"} ""]))))

(defpage [:get "/notes"] []
  (let [ notes (map #(assoc (dissoc % :id)
                            :uri
                            (str "/notes/" (:id %))) (note/all))]
    (response/json notes)))

(defpage [:post "/notes"] {:keys [body]}
  (let [ host ((:headers (request/ring-request)) "host")
         id (note/create! body)
         note (note/by-id id)
         generated-data (select-keys note [:created_at :updated_at])]
    (response/set-headers {"location" (str "http://" host "/notes/" id)}
      (response/status 201 (response/json generated-data)))))

(defpage [:get "/notes/:id"] {:keys [id]}
  (if-let [note (note/by-id id)]
          (response/json (dissoc (assoc note :uri (str "/notes/" id))
                                 :id))
          (response/status 404 (response/empty))))

(defpage [:put "/notes/:id"] {:as note}
  (let [row-count (note/update! (note :id)
                               (select-keys note [:body :created_at]))
        note (note/by-id (note :id))
        status (if (> row-count 0) 200 404)]
    (response/status status
                     (response/json (select-keys note [:updated_at])))))

(defpage [:delete "/notes/:id"] {:keys [id]}
  (let [row-count (note/delete! id)
        status (if (> row-count 0) 204 404)]
    (response/status status (response/empty))))
