(ns notes.views.templates)

(def templates (atom []))

(defmacro deftemplate
  "Defines an angularjs template in a script tag, which can be
   included in the original page load avoiding extra round trips when fetching templates."
  [name & body]
  `(swap! templates conj
         (conj [:script {:type "text/ng-template"
                         :id   ~name}]
                ~@body)))

(deftemplate "templates/note-list"
  [:div.note-list
    [:article.note
      {:ng-repeat "note in notes"
       :ng-click  "go(note.cid)"}
       [:div.note-inner-minimized {:ng-bind-html-unsafe "note.body"} ""]]])

(deftemplate "templates/note"
  [:article.note {:contenteditable true
                  :ng-model "note.body"}])

(deftemplate "templates/nav"
 [:nav {:ng-controller "ToolbarCtrl"}
         [:span {:ng-show "showBackButton()"}
           [:a {:ng-href "#/notes"} [:i.icon-btn.icon-chevron-left ""]]]
         [:span.right
           [:i.icon-btn.icon-save {:ng-show "canSave()"
                                   :ng-click "save()"} ""]
           [:i.icon-btn.icon-trash {:ng-click "trash()"
                                    :ng-show "canDelete()"} ""]
             [:a {:ng-href "#/notes/new"}
               [:i.icon-btn.icon-plus ""]]]])

(deftemplate "templates/popup"
  [:div.popup {:ng-show "popup.show"}
    [:div.popup-header "{{ popup.title }}"]
    [:div.popup-content "{{ popup.body }}"]
    [:div.popup-footer
      [:button.btn-gray {:ng-click "popup.cancel()"} "{{ popup.button1Text }}"]
      [:button {:ng-click "popup.done()"} "{{ popup.button2Text }}"]]]
  [:div.popup-backdrop {:ng-show "popup.show"}])
