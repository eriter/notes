(ns notes.models.note
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.java.jdbc.sql :as sql]
            [notes.db.schema :as schema]))

(defn now-utc-str []
  (let [formater (java.text.SimpleDateFormat. "yyyy-MM-dd HH:mm:ss")
        timezone (java.util.TimeZone/getTimeZone "UTC")]
    (do (.setTimeZone formater timezone)
        (.format formater (java.util.Date.)))))

(defn all []
  (jdbc/query schema/url
    (sql/select * :notes)))

(defn by-id [id]
  (first
    (jdbc/query schema/url
      (sql/select * :notes (sql/where {:id id})))))

(defn create! [content]
  (let [result (jdbc/insert! schema/url :notes
                            {:body content})]
  (->> result first first second)))

(defn update! [id values]
  (first (jdbc/update! schema/url
                       :notes
                       (assoc values
                              :updated_at
                              (now-utc-str))
                       (sql/where {:id id}))))

(defn delete! [id]
  (first (jdbc/delete! schema/url
                       :notes
                       (sql/where {:id id}))))
